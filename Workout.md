# 7 Day Split

**Mon** *- Legs & Abs*
  * Squats `(4x4)`
  * Calf Raise `(4x15)`

***

**Tue** *- Chest*
  * Bench Press `(4x8)`
  * Dumbell Fly `(3x10)`
  * Butterfly `(3x10)`

***

**Wed** *- Back & Abs*
  * Barbell Shrug `(3x15)`
  * Barbell Row `(3x15)`
  * Finish with pullups.

***

**Thu** *- Rest*

***

**Fri** *- Shoulders*
  * Upright barbell row `(4x10)`
  * Lateral raise `(4x10)`
  * Front Raise `(3x15)`
  * Finish with side Lateral.

***

**Sat** *- Biceps / Triceps*
  * Curls `(4 x 10)`
  * Triceps Pulldown `(4x10)`
  * Finish with Triceps Dips.

***

**Sun** *- Rest*
